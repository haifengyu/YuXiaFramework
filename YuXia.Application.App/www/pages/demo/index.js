﻿$(function () {
    $('#demolist .lr-nav-right').on('click', function () {
        var $this = $(this);
        var path = 'demo/' + $this.attr('data-value');
        var title = $this.text();
        YuXia.app.nav({ path: path, title: title, transition: 'right' });
    });

});