﻿using YuXia.Application.Base.OrganizationModule;
using YuXia.Application.Base.SystemModule;
using YuXia.Util;
using YuXia.Util.Operat;
using Nancy;

namespace YuXia.Application.WebApi
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创建人：雨夏-框架开发组
    /// 日 期：2017.05.12
    /// 描 述：用户信息
    /// </summary>
    public class UserApi : BaseApi
    {
        /// <summary>
        /// 注册接口
        /// </summary>
        public UserApi()
            : base("/YuXia/adms/user")
        {
            Post["/login"] = Login;
            Get["/info"] = Info;
        }
        private UserIBLL userBll = new UserBLL();
        private PostIBLL postIBLL = new PostBLL();
        private RoleIBLL roleIBLL = new RoleBLL();

        /// <summary>
        /// 登录接口
        /// </summary>
        /// <param name="_"></param>
        /// <returns></returns>
        private Response Login(dynamic _)
        {
            LoginModel loginModel = this.GetReqData<LoginModel>();

            #region 内部账户验证
            UserEntity userEntity = userBll.CheckLogin(loginModel.username, loginModel.password);

            #region 写入日志
            LogEntity logEntity = new LogEntity();
            logEntity.F_CategoryId = 1;
            logEntity.F_OperateTypeId = ((int)OperationType.Login).ToString();
            logEntity.F_OperateType = EnumAttribute.GetDescription(OperationType.Login);
            logEntity.F_OperateAccount = loginModel.username + "(" + userEntity.F_RealName + ")";
            logEntity.F_OperateUserId = !string.IsNullOrEmpty(userEntity.F_UserId) ? userEntity.F_UserId : loginModel.username;
            logEntity.F_Module = Config.GetValue("SoftName");
            #endregion

            if (!userEntity.LoginOk)//登录失败
            {
                //写入日志
                logEntity.F_ExecuteResult = 0;
                logEntity.F_ExecuteResultJson = "登录失败:" + userEntity.LoginMsg;
                logEntity.WriteLog();
                return Fail(userEntity.LoginMsg);
            }
            else
            {
                string token = OperatorHelper.Instance.AddLoginUser(userEntity.F_Account, "YuXia_ADMS_6.1_App", this.loginMark, false);//写入缓存信息
                //写入日志
                logEntity.F_ExecuteResult = 1;
                logEntity.F_ExecuteResultJson = "登录成功";
                logEntity.WriteLog();

                OperatorResult res = OperatorHelper.Instance.IsOnLine(token, this.loginMark);
                res.userInfo.password = null;
                res.userInfo.secretkey = null;
                var jsonData = new
                {
                    baseinfo = res.userInfo,
                    post = postIBLL.GetListByPostIds(res.userInfo.postIds),
                    role = roleIBLL.GetListByRoleIds(res.userInfo.roleIds)
                };
                return Success(jsonData);
            }
            #endregion
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="_"></param>
        /// <returns></returns>
        private Response Info(dynamic _)
        {
            var data = userInfo;
            data.password = null;
            data.secretkey = null;

            var jsonData = new
            {
                baseinfo = data,
                post = postIBLL.GetListByPostIds(data.postIds),
                role = roleIBLL.GetListByRoleIds(data.roleIds)
            };

            return Success(jsonData);
        }

        /// <summary>
        /// 获取所有员工账号列表
        /// </summary>
        /// <param name="_"></param>
        /// <returns></returns>
        private Response GetList(dynamic _) {
            var data = userInfo;
            data.password = null;
            data.secretkey = null;
            var jsonData = new
            {
                baseinfo = data,
                post = postIBLL.GetListByPostIds(data.postIds),
                role = roleIBLL.GetListByRoleIds(data.roleIds)
            };
            return Success(jsonData);
        }
    }

    /// <summary>
    /// 登录信息
    /// </summary>
    public class LoginModel {
        /// <summary>
        /// 账号
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string password { get; set; }
    }
}