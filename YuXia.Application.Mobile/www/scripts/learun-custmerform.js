﻿/*
 * 版 本 YuXia-Mobile V1.0.0 力软敏捷开发框架(http://www.YuXia.cn)
 * Copyright (c) 2013-2017 上海力软信息技术有限公司
 * 创建人：力软-前端开发组
 * 日 期：2017.12.15
 * 描 述：力软移动端框架 自定义表单处理方法
 */
(function ($, YuXia, window) {
    // 自定义表单初始化
    $.fn.custmerform = function (data,type) {// 表单数据 0发起流程 1 审核流程
        YuXia.layer.loading(true, "正在加载表单数据");
        var res = true;
        var $this = $(this);
        var formids = [];
        var formdata = {};
        // 判断下是否有系统表单（暂时不支持系统表单）
        for (var i = 0, l = data.length; i < l; i++) {
            var formItem = data[i];
            if (formItem.formId == "") {// 如果是系统表单就返回错误（目前暂时不支持）
                return false;
            }
            formdata[formItem.formId] = formItem;
            formids.push(formItem.formId);
            //console.log(formItem,'formItem');
        }
        // 加载自定义表单模板
        YuXia.httpget(config.webapi + "/YuXia/adms/form/scheme", String(formids), (res) => {
            console.log(res);
            if (res.code == 200) {// 加载表单
                $this.scroll();
                var $container = $this.find('.f-scroll>div');
                $.each(res.data, function (_index, _item) {
                    var formScheme = JSON.parse(_item.F_Scheme);
                    //F_SchemeInfoId
                    custmerformRender($container, formScheme.data, _item.F_SchemeInfoId);
                    //console.log(_item);
                    //console.log(JSON.parse(_item.F_Scheme))
                });
                if (type == 0) {
                    // 加载备注输入框
                    $container.append('\
                    <div class="lr-form-container" >\
                        <div class="lr-form-row lr-form-row-title"  >\
                            <label style="width:200px" >流程自定义信息</label>\
                        </div>\
                        <div class="lr-form-row">\
                            <label>流程标题</label>\
                            <input id="lrflowworktitle"   type="text" >\
                        </div>\
                        <div class="lr-form-row">\
                            <label>重要等级</label>\
                            <div id="lrflowworklevel"></div>\
                        </div>\
                        <div class="lr-form-row lr-form-row-multi">\
                            <label>备注</label>\
                            <div id="lrflowworkdes"  class="lrtextarea"  contenteditable="true" ></div>\
                        </div>\
                    </div>');
                    var d = $('#lrflowworklevel').lrpicker({
                        data: [
                            { 'text': '普通', 'value': '0' },
                            { 'text': '重要', 'value': '1' },
                            { 'text': '紧急', 'value': '2' }
                        ]
                    }).lrpickerSet('1');
                } else if (type == 1) {
                    // 加载备注输入框
                    $container.append('\
                    <div class="lr-form-container" >\
                        <div class="lr-form-row lr-form-row-title"  >\
                            <label style="width:200px" >流程审核信息</label>\
                        </div>\
                        <div class="lr-form-row">\
                            <label>审核结果</label>\
                            <div id="lrflowworkverify"></div>\
                        </div>\
                        <div class="lr-form-row lr-form-row-multi">\
                            <label>备注</label>\
                            <div id="lrflowworkdes"  class="lrtextarea"  contenteditable="true" ></div>\
                        </div>\
                    </div>');
                    var d = $('#lrflowworkverify').lrpicker({
                        data: [
                            { 'text': '同意', 'value': '1' },
                            { 'text': '不同意', 'value': '2' }
                        ]
                    });
                }
            }
        });

        return res;
    }
    // 获取自定义表单数据
    $.fn.custmerformGet = function () {
        var res = {};
        $(this).find('.lrcomponts').each(function () {
            var $this = $(this);
            var schemeInfoId = $this.attr('data-id');
            var _componts = $this[0].componts;
            res[schemeInfoId] = res[schemeInfoId] || {};
            // 遍历自定义表单控件
            $.each(_componts, function (_index, _item) {
                var _fn = componts[_item.type].get;
                if (!!_fn) {
                    var value = _fn(_item);
                    res[schemeInfoId][_item.id] = value;
                }
            });
            $this = null;
        });
        return res;
    }
    // 设置自定义表单数据
    $.fn.custmerformSet = function (data) {
        var $this = $(this);
        function set($this, data) {
            console.log('ceshi');
            if ($this.find('.lrcomponts').length > 0) {
                $this.find('.lrcomponts').each(function () {
                    var $this = $(this);
                    var schemeInfoId = $this.attr('data-id');
                    var _componts = $this[0].componts;
                    var _data = {};
                    $.each(data[schemeInfoId], function (_index, _item) {
                        $.each(_item, function (_id, _jitem) {
                            $.extend(_data, _jitem);
                        });
                    });
                    //console.log(_data);

                    // 遍历自定义表单控件
                    $.each(_componts, function (_index, _item) {
                        var _fn = componts[_item.type].set;
                        if (!!_fn) {
                            //console.log(_item.id.replace(/-/g, '_'), _data[('lr' + _item.id.replace(/-/g, '_'))]);
                            _fn(_item, _data[('lr' + _item.id.replace(/-/g, '_'))]);
                        }
                    });
                    $this = null;
                });
            }
            else {
                setTimeout(function () {
                    set($this, data);
                }, 100);
            }
        }
        set($this, data);
    }

    // 渲染自定义表单
    function custmerformRender($container, scheme, schemeInfoId) {
        var loaddataComponts = [];
        $.each(scheme, function (_index, _item) {
            var $list = $('<div class="lr-form-container lrcomponts" data-id="' + schemeInfoId + '" ></div>');
            $list[0].componts = _item.componts;
            $.each(_item.componts, function (_jindex, _jitem) {
                var $row = $('<div class="lr-form-row"><label>' + _jitem.title + '</label></div>');
                if (componts[_jitem.type].render($row, _jitem)) {
                    $list.append($row);
                }
                // 记录需要获取后台数据的控件，统一初始化
                //if (_jitem.type == 'radio') {
                //    loaddataComponts.push(_jitem);
                //}
                //console.log(_jitem);
            });
            $container.append($list);
        });
    }

    var componts = {
        label: {
            render: function ($row, compont) {
                $row.addClass('lr-form-row-title');
                return true;
            }
        },
        html: {
            render: function ($row, compont) {// 移动端暂时不支持
                return false;
            }
        },
        text: {
            render: function ($row, compont) {
                var $compont = $('<input id="' + compont.id + '" type="text" />');
                $compont.val(compont.dfvalue);
                $row.append($compont);
                $compont = null;
                return true;
            },
            get: function (compont) {
                return $('#' + compont.id).val();
            },
            set: function (compont, value) {
                $('#' + compont.id).val(value);
            }
        },
        textarea: {
            render: function ($row, compont) {
                $row.addClass('lr-form-row-multi');
                var $compont = $('<div id="' + compont.id + '"  class="lrtextarea"  contenteditable="true" ></div>');
                $compont.text(compont.dfvalue);
                $row.append($compont);
                $compont = null;
                return true;
            },
            get: function (compont) {
                return $('#' + compont.id).text();
            },
            set: function (compont, value) {
                if (!!value) {
                    $('#' + compont.id).text(value);
                }
            }
        },
        texteditor: {
            render: function ($row, compont) {// 移动端富文本和普通多行文本一致
                $row.addClass('lr-form-row-multi');
                var $compont = $('<div id="' + compont.id + '"  class="lrtextarea"  contenteditable="true" ></div>');
                $compont.text(compont.dfvalue);
                $row.append($compont);
                $compont = null;
                return true;
            },
            get: function (compont) {
                return $('#' + compont.id).text();
            },
            set: function (compont, value) {
                if (!!value) {
                    $('#' + compont.id).text(value);
                }
            }
        },
        radio: {
            render: function ($row, compont) {// 单选改用和下拉一致
                var $compont = $('<div id="' + compont.id + '" ></div>');
                $row.append($compont);
                $compont = null;
                // 获取数据
                if (compont.dataSource == '0') {
                    YuXia.getDataItem(compont.itemCode, function (data) {
                        $('#' + compont.id).lrpicker({
                            data: data,
                            ivalue: 'F_ItemValue',
                            itext: 'F_ItemName'
                        });
                    });
                }
                else {
                    var vlist = compont.dataSourceId.split(',');
                    YuXia.getDataItem(vlist[0], function (data) {
                        $('#' + compont.id).lrpicker({
                            data: data,
                            ivalue: vlist[2],
                            itext: vlist[1]
                        });

                    });
                }
                return true;
            },
            get: function (compont) {
                return $('#' + compont.id).lrpickerGet();
            },
            set: function (compont, value) {
                $('#' + compont.id).lrpickerSet(value);
            }
        },
        checkbox: {
            render: function ($row, compont) {
                $row.addClass('lr-form-row-title');
                $row.attr('id', compont.id);
                if (compont.dataSource == '0') {
                    YuXia.getDataItem(compont.itemCode, function (data) {
                        var _$row = $('#' + compont.id);
                        $.each(data, function (_index, _item) {
                            var _$div = $('<div class="lr-form-row" data-name="' + compont.id + '" data-value="' + _item.F_ItemValue +'" ><label>' + _item.F_ItemName + '</label><div class="checkbox" ></div></div>');;
                            _$row.after(_$div);
                            _$div.find('.checkbox').lrswitch();
                            _$div = null;
                        });
                        _$row = null;
                    });
                }
                else {
                    var vlist = compont.dataSourceId.split(',');
                    YuXia.getDataItem(vlist[0], function (data) {
                        var _$row = $('#' + compont.id);
                        $.each(data, function (_index, _item) {
                            var _$div = $('<div class="lr-form-row" data-name="' + compont.id + '" data-value="' + _item[vlist[2]] + '" ><label>' + _item[vlist[1]] + '</label><div class="checkbox" ></div></div>');;
                            _$row.after(_$div);
                            _$div.find('.checkbox').lrswitch();
                            _$div = null;
                        });
                        _$row = null;
                    });
                }
                return true;
            },
            get: function (compont) {
                var values = [];
                $('[data-name=' + compont.id + '"]').each(function () {
                    if ($(this).lrswitchGet() == 1) {
                        var v = $(this).attr('data-value');
                        values.push(v);
                    }
                });
                return String(values);
            },
            set: function (compont, value) {
                var values = value.split(',');
                $.each(values, function (_index, _item) {
                    $('[data-name=' + compont.id + '"][data-value="' + _item + '"]').lrswitchSet(1);
                });
            }
        },
        select: {
            render: function ($row, compont) {//
                var $compont = $('<div id="' + compont.id + '" ></div>');
                $row.append($compont);
                $compont = null;
                // 获取数据
                if (compont.dataSource == '0') {
                    YuXia.getDataItem(compont.itemCode, function (data) {
                        $('#' + compont.id).lrpicker({
                            data: data,
                            ivalue: 'F_ItemValue',
                            itext: 'F_ItemName'
                        });
                    });
                }
                else {
                    var vlist = compont.dataSourceId.split(',');
                    YuXia.getDataItem(vlist[0], function (data) {
                        $('#' + compont.id).lrpicker({
                            data: data,
                            ivalue: vlist[2],
                            itext: vlist[1]
                        });

                    });
                }
                return true;
            },
            get: function (compont) {
                return $('#' + compont.id).lrpickerGet();
            },
            set: function (compont, value) {
                $('#' + compont.id).lrpickerSet(value);
            }
        },
        datetime: {
            render: function ($row, compont) {//
                var $compont = $('<div id="' + compont.id + '" ></div>');
                $row.append($compont);
                if (compont.dateformat == '0') {
                    $compont.lrdate({
                        type: 'date',
                    });
                }
                else {
                    $compont.lrdate();
                }
                $compont = null;
                return true;
            },
            get: function (compont) {
                return $('#' + compont.id).lrdateGet();
            },
            set: function (compont, value) {
                if (compont.dateformat == '0') {
                    value = YuXia.date.format(value,'yyyy-MM-dd');
                }
                else {
                    value = YuXia.date.format(value, 'yyyy-MM-dd hh:mm');
                }

                $('#' + compont.id).lrdateSet(value);
            }
        },
        datetimerange: {
            render: function ($row, compont) {//
                var $compont = $('<input id="' + compont.id + '" type="text" />');
                function register() {
                    if ($('#' + compont.startTime).length > 0 && $('#' + compont.endTime).length > 0) {
                        $('#' + compont.startTime).on('change', function () {
                            var st = $(this).lrdateGet();
                            var et = $('#' + compont.endTime).lrdateGet();
                            if (!!st && !!et) {
                                var diff = YuXia.date.parse(st).DateDiff('d', et) + 1;
                                $('#' + compont.id).val(diff);
                            }
                        });
                        $('#' + compont.endTime).on('change', function () {
                            var st = $('#' + compont.startTime).lrdateGet();
                            var et = $(this).lrdateGet();
                            if (!!st && !!et) {
                                var diff = YuXia.date.parse(st).DateDiff('d', et) + 1;
                                $('#' + compont.id).val(diff);
                            }
                        });
                    }
                    else {
                        setTimeout(function () {
                            register();
                        }, 50);
                    }
                }
                if (!!compont.startTime && compont.endTime) {
                    register();
                }
                $row.append($compont);
                $compont = null;
                return true;
            },
            get: function (compont) {
                return $('#' + compont.id).val();
            },
            set: function (compont, value) {
                $('#' + compont.id).val(value);
            }
        },
        encode: {
            render: function ($row, compont) {
                var $compont = $('<input id="' + compont.id + '" type="text" readonly  />');
                compont.isInit = false;
                YuXia.getRuleCode(compont.rulecode, function (data) {
                    if (!compont.isInit) {
                        compont.isInit = true;
                        $('#' + compont.id).val(data);
                    }
                });
                $row.append($compont);
                $compont = null;
                return true;
            },
            get: function (compont) {
                return $('#' + compont.id).val();
            },
            set: function (compont, value) {
                compont.isInit = true;
                $('#' + compont.id).val(value);
            }
        },
        organize: {
            render: function ($row, compont) {
                return false;

                var $compont = $('<div id="' + compont.id + '" ></div>');
                $row.append($compont);
                $compont = null;
                switch (compont.dataType) {
                    case "user"://用户
                        if (compont.relation != "") {
                            $compont.lrselect({
                                value: 'F_UserId',
                                text: 'F_RealName',
                                title: 'F_RealName',
                                // 展开最大高度
                                maxHeight: compont.height,
                                // 是否允许搜索
                                allowSearch: true
                            });
                            function register() {
                                if ($('#' + compont.relation).length > 0) {
                                    $('#' + compont.relation).on('change', function () {
                                        var value = $(this).lrselectGet();
                                        if (value == "") {
                                            $compont.lrselectRefresh({
                                                url: '',
                                                data: []
                                            });
                                        }
                                        else {
                                            $compont.lrselectRefresh({
                                                url: top.$.rootUrl + '/LR_OrganizationModule/User/GetList',
                                                param: { departmentId: value }
                                            });
                                        }
                                    });
                                }
                                else {
                                    setTimeout(function () { register(); }, 100);
                                }
                            }
                            register();
                        }
                        else {
                            $compont.lrformselect({
                                layerUrl: top.$.rootUrl + '/LR_OrganizationModule/User/SelectOnlyForm',
                                layerUrlW: 400,
                                layerUrlH: 300,
                                dataUrl: top.$.rootUrl + '/LR_OrganizationModule/User/GetListByUserIds'
                            });
                        }
                        break;
                    case "department"://部门
                        $compont.lrselect({
                            type: 'tree',
                            // 展开最大高度
                            maxHeight: compont.height,
                            // 是否允许搜索
                            allowSearch: true
                        });
                        if (compont.relation != "") {
                            function register() {
                                if ($('#' + compont.relation).length > 0) {
                                    $('#' + compont.relation).on('change', function () {
                                        var value = $(this).lrselectGet();
                                        $compont.lrselectRefresh({
                                            url: top.$.rootUrl + '/LR_OrganizationModule/Department/GetTree',
                                            param: { companyId: value }
                                        });
                                    });
                                }
                                else {
                                    setTimeout(function () { register(); }, 100);
                                }
                            }
                            register();
                        }
                        else {
                            $compont.lrselectRefresh({
                                url: top.$.rootUrl + '/LR_OrganizationModule/Department/GetTree',
                                param: {}
                            });
                        }
                        break;
                    case "company"://公司
                        $compont.lrCompanySelect({ maxHeight: compont.height });
                        break;
                }
                return true;
            }
        },
        currentInfo: {
            render: function ($row, compont) {
                var $compont = $('<input id="' + compont.id + '" readonly type="text"  />');
                var userinfo = YuXia.storage.get('userinfo');
                var organization = YuXia.storage.get('organization');

                switch (compont.dataType) {
                    case 'company':
                        compont.value = userinfo.baseinfo.companyId;
                        $compont.val(organization[compont.value].name);
                        break;
                    case 'department':
                        compont.value = userinfo.baseinfo.departmentId;
                        $compont.val(organization[compont.value].name);
                        break;
                    case 'user':
                        $compont.val(userinfo.baseinfo.realName);
                        compont.value = userinfo.baseinfo.userId;
                        break;
                    case 'time':
                        compont.value = YuXia.date.format(new Date(), 'yyyy-MM-dd hh:mm:ss');
                        $compont.val(compont.value);
                        break;
                    case 'guid':
                        compont.value = YuXia.guid();
                        $compont.val(compont.value);
                        break;
                }
                if (compont.isHide == '1') {
                    return false;
                }
                else {
                    $row.append($compont);
                    $compont = null;
                }
                return true;
            },
            get: function (compont) {
                return compont.value;
            },
            set: function (compont, value) {
                //var userinfo = YuXia.storage.get('userinfo');
                if (!!value) {
                    var organization = YuXia.storage.get('organization');

                    switch (compont.dataType) {
                        case 'company':
                            compont.value = value;
                            if (compont.isHide != '1') {
                                $('#' + compont.id).val(organization[value].name);
                            }
                            break;
                        case 'department':
                            compont.value = value;
                            if (compont.isHide != '1') {
                                $('#' + compont.id).val(organization[value].name);
                            }
                            break;
                        case 'user':
                            compont.value = value;
                            if (compont.isHide != '1') {
                                if (value == "System") {
                                    $('#' + compont.id).val('超级管理员');
                                }
                                else {
                                    $('#' + compont.id).val(organization[value].name);
                                }
                            }
                            break;
                        case 'time':
                            compont.value = value;
                            if (compont.isHide != '1') {
                                $('#' + compont.id).val(value);
                            }
                            break;
                        case 'guid':
                            compont.value = value;
                            if (compont.isHide != '1') {
                                $('#' + compont.id).val(value);
                            }
                            break;
                    }
                }
            }
        },
        guid: {
            render: function ($row, compont) {
                compont.value = YuXia.guid();
                $row.remove();
                return false;
            },
            get: function (compont) {
                return compont.value;
            },
            set: function (compont, value) {
                compont.value = value;
            }
        },
        upload: {
            render: function () {
                return false;
            }
        },
        girdtable: {
            render: function () {
                return false;
            }
        }
    }

    

})(window.jQuery, window.lrmui, window);

