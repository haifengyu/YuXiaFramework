﻿(function () {
    var page = {
        init: function ($page) {
            $page.find('#loginBtn').on('tap', function () {
                var account = $('#account').val();
                var password = $('#password').val();
                
                if (account == "") {
                    YuXia.layer.warning('用户名不能为空！', function () { }, '力软提示', '关闭');
                } else if (password == "") {
                    YuXia.layer.warning('密码不能为空！', function () { }, '力软提示', '关闭');
                } else {
                    var data = {
                        username: account,
                        password: $.md5(password)
                    }
                    var postdata = {
                        token: '',
                        loginMark: YuXia.deviceId,// 正式请换用设备号
                        data: JSON.stringify(data)
                    }
                    var path = config.webapi;
                    YuXia.layer.loading(true, "正在登录，请稍后");
                    YuXia.http.post(path + "/YuXia/adms/user/login", postdata, (res) => {
                        YuXia.layer.loading(false);
                        if (res.code == "200") {
                            var logininfo = {
                                account: account,
                                token: res.data.baseinfo.token,
                                date: YuXia.date.format(new Date(),'yyyy-MM-dd hh:mm:ss')
                            };
                            YuXia.storage.set('logininfo', logininfo);
                            // 保存登录信息
                            YuXia.storage.set('userinfo', res.data);

                            YuXia.tab.go('workspace');
                        } else {
                            YuXia.layer.warning(res.info, function () { }, '力软提示', '关闭');
                        }
                    });
                } 
            });


        }
    };
    return page;
})();
