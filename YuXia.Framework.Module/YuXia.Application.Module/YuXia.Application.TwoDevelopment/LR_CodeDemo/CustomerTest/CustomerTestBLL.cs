﻿using YuXia.Util;
using System;
using System.Collections.Generic;

namespace YuXia.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-20 15:07
    /// 描 述：客户类测试
    /// </summary>
    public class CustomerTestBLL : CustomerTestIBLL
    {
        private CustomerTestService customerTestService = new CustomerTestService();

        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<LR_CRM_CustomerEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                return customerTestService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取LR_CRM_Customer表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public LR_CRM_CustomerEntity GetLR_CRM_CustomerEntity(string keyValue)
        {
            try
            {
                return customerTestService.GetLR_CRM_CustomerEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取LR_CRM_CustomerContact表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public LR_CRM_CustomerContactEntity GetLR_CRM_CustomerContactEntity(string keyValue)
        {
            try
            {
                return customerTestService.GetLR_CRM_CustomerContactEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                customerTestService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, LR_CRM_CustomerEntity entity,LR_CRM_CustomerContactEntity lR_CRM_CustomerContactEntity)
        {
            try
            {
                customerTestService.SaveEntity(keyValue, entity,lR_CRM_CustomerContactEntity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
