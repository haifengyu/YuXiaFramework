﻿using YuXia.Application.Base.SystemModule;
using Nancy;

namespace YuXia.Application.WebApi.Modules
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创建人：雨夏-框架开发组
    /// 日 期：2018.01.04
    /// 描 述：数据源接口
    /// </summary>s
    public class DataSourceApi: BaseApi
    {
        /// <summary>
        /// 注册接口
        /// </summary>
        public DataSourceApi()
            : base("/YuXia/adms/datasource")
        {
            Get["/data"] = GetDataTable;// 获取数据表数据
        }
        DataSourceIBLL dataSourceIBLL = new DataSourceBLL();
        /// <summary>
        /// 获取数据表数据
        /// </summary>
        /// <param name="_"></param>
        /// <returns></returns>
        private Response GetDataTable(dynamic _)
        {
            string req = this.GetReqData();// 获取模板请求数据
            var data = dataSourceIBLL.GetDataTable(req, "");
            return Success(data);
        }
    }
}