﻿namespace YuXia.Util
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创建人：雨夏-框架开发组
    /// 日 期：2017.03.07
    /// 描 述：mvc过滤模式
    /// </summary>
    public enum FilterMode
    {
        /// <summary>执行</summary>
        Enforce,
        /// <summary>忽略</summary>
        Ignore
    }
}
