﻿using Dapper;
using YuXia.DataBase.Repository;
using YuXia.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace YuXia.Application.TwoDevelopment.LR_CodeDemo
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创 建：超级管理员
    /// 日 期：2018-08-20 15:07
    /// 描 述：客户类测试
    /// </summary>
    public class CustomerTestService : RepositoryFactory
    {
        #region 获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<LR_CRM_CustomerEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_CustomerId,
                t.F_CustomerId,
                t.F_EnCode,
                t.F_FullName,
                t.F_ShortName,
                t.F_CustIndustryId,
                t.F_CustTypeId,
                t.F_CustLevelId,
                t.F_Province,
                t.F_City,
                t.F_Contact,
                t.F_Mobile,
                t.F_Tel
                ");
                strSql.Append("  FROM LR_CRM_Customer t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                return this.BaseRepository().FindList<LR_CRM_CustomerEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取LR_CRM_Customer表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public LR_CRM_CustomerEntity GetLR_CRM_CustomerEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<LR_CRM_CustomerEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取LR_CRM_CustomerContact表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public LR_CRM_CustomerContactEntity GetLR_CRM_CustomerContactEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<LR_CRM_CustomerContactEntity>(t=>t.F_CustomerId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var lR_CRM_CustomerEntity = GetLR_CRM_CustomerEntity(keyValue); 
                db.Delete<LR_CRM_CustomerEntity>(t=>t.F_CustomerId == keyValue);
                db.Delete<LR_CRM_CustomerContactEntity>(t=>t.F_CustomerId == lR_CRM_CustomerEntity.F_CustomerId);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, LR_CRM_CustomerEntity entity,LR_CRM_CustomerContactEntity lR_CRM_CustomerContactEntity)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    var lR_CRM_CustomerEntityTmp = GetLR_CRM_CustomerEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    db.Delete<LR_CRM_CustomerContactEntity>(t=>t.F_CustomerId == lR_CRM_CustomerEntityTmp.F_CustomerId);
                    lR_CRM_CustomerContactEntity.Create();
                    lR_CRM_CustomerContactEntity.F_CustomerId = lR_CRM_CustomerEntityTmp.F_CustomerId;
                    db.Insert(lR_CRM_CustomerContactEntity);
                }
                else
                {
                    entity.Create();
                    db.Insert(entity);
                    lR_CRM_CustomerContactEntity.Create();
                    lR_CRM_CustomerContactEntity.F_CustomerId = entity.F_CustomerId;
                    db.Insert(lR_CRM_CustomerContactEntity);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
