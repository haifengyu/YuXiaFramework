﻿/*
 * 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架(http://www.YuXia.cn)
 * Copyright (c) 2013-2017 上海雨夏软件科技有限公司
 * 创建人：雨夏-前端开发组
 * 日 期：2017.04.11
 * 描 述：数据源管理	
 */
var keyValue = "";
var acceptClick;
var bootstrap = function ($, YuXia) {
    "use strict";
    var selectedRow = YuXia.frameTab.currentIframe().selectedRow;
    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            $('#F_DbId').lrDbSelect();
        },
        initData: function () {
            if (!!selectedRow) {
                keyValue = selectedRow.F_Id;
                YuXia.httpAsync('GET', top.$.rootUrl + '/LR_SystemModule/DataSource/GetEntityByCode', { keyValue: selectedRow.F_Code }, function (data) {
                    $('#form').lrSetFormData(data);
                });
            }
        }
    };
    // 保存数据
    acceptClick = function (callBack) {
        if (!$('#form').lrValidform()) {
            return false;
        }
        var postData = $('#form').lrGetFormData(keyValue);
        $.lrSaveForm(top.$.rootUrl + '/LR_SystemModule/DataSource/SaveForm?keyValue=' + keyValue, postData, function (res) {
            // 保存成功后才回调
            if (!!callBack) {
                callBack();
            }
        });
    };
    page.init();
}