﻿namespace YuXia.Util
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创建人：雨夏-框架开发组
    /// 日 期：2017.07.10
    /// 描 述：表格属性模型
    /// </summary>
    public class jfGridModel
    {
        public string name { get; set; }
        public string label { get; set; }
        public string width { get; set; }
        public string align { get; set; }
        public string height { get; set; }
        public string hidden { get; set; }
    }
}
