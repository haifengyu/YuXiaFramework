﻿// 有关“空白”模板的简介，请参阅以下文档:
// http://go.microsoft.com/fwlink/?LinkID=397704
// 若要在 cordova-simulate 或 Android 设备/仿真器上在页面加载时调试代码: 启动应用，设置断点，
// 然后在 JavaScript 控制台中运行 "window.location.reload()"。
(function ($, YuXia) {
    "use strict";

    // 封装一层http请求，带上用户信息
    YuXia.httpget = function (url, data, callback) {
        var param = {};
        var logininfo = YuXia.storage.get('logininfo');
        param.token = logininfo.token;
        param.loginMark = YuXia.deviceId;
        var type = YuXia.type(data);
        if (type == 'object' || type == 'array') {
            param.data = JSON.stringify(data);
        }
        else if (type == 'string') {
            param.data = data;
        }

        return YuXia.http.get(url, param, callback);
    };
    YuXia.httppost = function (url, data, callback) {
        var param = {};
        var logininfo = YuXia.storage.get('logininfo');
        param.token = logininfo.token;
        param.loginMark = YuXia.deviceId;
        var type = YuXia.type(data);
        console.log(type);
        if (type == 'object' || type == 'array') {
            param.data = JSON.stringify(data);
        }
        else if (type == 'string') {
            param.data = data;
        }

        return YuXia.http.post(url, param, callback);
    };

    // 获取数据字典数据
    YuXia.getDataItem = function (code, callback) {
        YuXia.httpget(config.webapi + "/YuXia/adms/dataitem/details",code, function (res) {
            if (res.code == 200) {
                callback(res.data);
            }
            else {
                callback([]);
            }
        });
    }
    // 获取数据源数据
    YuXia.getDataSource = function (code, callback) {
        YuXia.httpget(config.webapi + "/YuXia/adms/datasource/data",code, function (res) {
            if (res.code == 200) {
                callback(res.data);
            }
            else {
                callback([]);
            }
        });
    }
    // 获取单据编码
    YuXia.getRuleCode = function (code, callback) {
        YuXia.httpget(config.webapi + "/YuXia/adms/coderule/code", code, function (res) {
            if (res.code == 200) {
                callback(res.data);
            }
            else {
                callback('');
            }
        });
    }
    // 获取下公司信息
    YuXia.getOrganization = function (callback) {
        YuXia.httpget(config.webapi + "/YuXia/adms/organization/map", null, function (res) {
            console.log(res, 'organization');
            if (res.code == 200) {
                YuXia.storage.set('organization', res.data);
                callback();
            }
            else {
                callback();
            }
        });
    }



    // 初始化页面
    var tabdata = [
        {
            page: 'workspace',
            text: '工作区',
            icon: 'icon-appfill',
            fillicon: 'icon-app'
        },
        {
            page: 'my',
            text: '我的',
            icon: 'icon-people',
            fillicon: 'icon-peoplefill'
        }
    ];
    YuXia.init(function () {
        // 处理 Cordova 暂停并恢复事件
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        YuXia.tab.init(tabdata);

        var logininfo = YuXia.storage.get('logininfo');
        if (!!logininfo) {// 有登录的token
            YuXia.httpget(config.webapi + "/YuXia/adms/user/info", {}, (res) => {
                console.log(res);
                if (res.code == 200) {
                    YuXia.storage.set('userinfo', res.data);
                    YuXia.tab.go('workspace');
                    YuXia.getOrganization(function () {
                        YuXia.splashscreen.hide();
                    });
                }
                else {
                    YuXia.nav.go({ path: 'login', isBack: false, isHead: false });
                    YuXia.splashscreen.hide();
                }                
            });
        }
        else {
            YuXia.nav.go({ path: 'login', isBack: false, isHead: false });
            YuXia.splashscreen.hide();
        }
       
    });

    function onPause() {
        // TODO: 此应用程序已挂起。在此处保存应用程序状态。
    };

    function onResume() {
        // TODO: 此应用程序已重新激活。在此处还原应用程序状态。
    };

})(window.jQuery, window.lrmui);