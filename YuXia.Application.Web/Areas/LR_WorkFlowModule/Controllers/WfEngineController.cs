﻿using YuXia.Application.Base.AuthorizeModule;
using YuXia.Application.Base.OrganizationModule;
using YuXia.Util;
using System.Collections.Generic;
using System.Web.Mvc;

namespace YuXia.Application.Web.Areas.LR_WorkFlowModule.Controllers
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创建人：雨夏-框架开发组
    /// 日 期：2018.01.16
    /// 描 述：流程引擎（流程接口）
    /// </summary>
    public class WfEngineController : MvcControllerBase
    {
        private UserRelationIBLL userRelationIBLL = new UserRelationBLL();
        private UserIBLL userIBLL = new UserBLL();

        #region 获取数据
        /// <summary>
        /// 初始化流程模板->获取开始节点数据
        /// </summary>
        /// <param name="isNew">是否是新发起的实例</param>
        /// <param name="processId">流程实例ID</param>
        /// <param name="schemeCode">流程模板编码</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult Bootstraper(bool isNew, string processId, string schemeCode)
        {
            UserInfo userInfo = LoginUserInfo.Get();
            string url = Config.GetValue("workflowapi");
            var data = new
            {
                isNew = isNew,
                processId = processId,
                schemeCode = schemeCode
            };
            url = url + "/workflow/bootstraper?token=" + userInfo.token + "&loginMark=" + userInfo.loginMark + "&data=" + data.ToJson();
            string res = HttpMethods.Get(url);
            return Content(res);
        }
        /// <summary>
        /// 流程任务信息
        /// </summary>
        /// <param name="processId">流程实例ID</param>
        /// <param name="taskId">流程模板编码</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult Taskinfo(string processId, string taskId)
        {
            UserInfo userInfo = LoginUserInfo.Get();
            string url = Config.GetValue("workflowapi");
            var data = new
            {
                processId = processId,
                taskId = taskId
            };
            url = url + "/workflow/taskinfo?token=" + userInfo.token + "&loginMark=" + userInfo.loginMark + "&data=" + data.ToJson();
            string res = HttpMethods.Get(url);
            return Content(res);
        }
        /// <summary>
        /// 获取流程实例信息
        /// </summary>
        /// <param name="processId">流程实例ID</param>
        /// <param name="taskId">流程模板编码</param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult Processinfo(string processId, string taskId)
        {

            UserInfo userInfo = LoginUserInfo.Get();
            string url = Config.GetValue("workflowapi");
            var data = new
            {
                processId = processId,
                taskId = taskId
            };
            url = url + "/workflow/processinfo?token=" + userInfo.token + "&loginMark=" + userInfo.loginMark + "&data=" + data.ToJson();
            string res = HttpMethods.Get(url);
            return Content(res);
        }
        /// <summary>
        /// 获取下一个节点审核人员
        /// </summary>
        /// <param name="processId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet]
        [AjaxOnly]
        public ActionResult Auditer(bool isNew, string processId, string schemeCode, string taskId,string formData)
        {

            UserInfo userInfo = LoginUserInfo.Get();
            string url = Config.GetValue("workflowapi");
            var data = new
            {
                isNew = isNew,
                processId = processId,
                schemeCode = schemeCode,
                taskId = taskId,
                formData = formData
            };
            url = url + "/workflow/auditer?token=" + userInfo.token + "&loginMark=" + userInfo.loginMark + "&data=" + data.ToJson();
            string res = HttpMethods.Get(url);
            // 将角色和刚位转化成人员列表
            var obj = res.ToJObject();
            if (obj["code"].ToInt() == 200 && obj["data"]["status"].ToInt() == 1)
            {
                List<object> nodelist = new List<object>();
                var list = obj["data"]["data"];
                foreach (var item in list) {
                    if (item["auditors"].IsEmpty())
                    {
                        var point = new
                        {
                            all = true,
                            name = item["name"],
                            nodeId = item["nodeId"]
                        };
                        nodelist.Add(point);
                    }
                    else {
                        List<object> userlist = new List<object>();
                        foreach (var auditor in item["auditors"]) {
                            switch (auditor["type"].ToString()) {//获取人员信息1.岗位2.角色3.用户
                                case "1":
                                case "2":
                                    var userRelationList = userRelationIBLL.GetUserIdList(auditor["auditorId"].ToString());
                                    string userIds = "";
                                    foreach (var userRelation in userRelationList)
                                    {
                                        if (userIds != "")
                                        {
                                            userIds += ",";
                                        }
                                        userIds += userRelation.F_UserId;
                                    }
                                    var userList = userIBLL.GetListByUserIds(userIds);
                                    foreach (var user in userList) {
                                        if (user != null) {
                                            userlist.Add(new { id = user.F_UserId, name = user.F_RealName });
                                        }
                                    }
                                    break;
                                case "3":
                                    userlist.Add(new { id = auditor["auditorId"], name = auditor["auditorName"] });
                                    break;
                            }
                        }
                        var point = new
                        {
                            name = item["name"],
                            nodeId = item["nodeId"],
                            list = userlist
                        };
                        nodelist.Add(point);
                    }
                }

                return Success(nodelist);
            }
            else
            {
                return Fail("获取数据失败！");
            }


        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 创建流程实例
        /// </summary>
        /// <param name="isNew">是否是新发起的实例</param>
        /// <param name="processId">流程实例ID</param>
        /// <param name="schemeCode">流程模板编码</param>
        /// <param name="processName">流程实例名称</param>
        /// <param name="processLevel">流程重要等级</param>
        /// <param name="description">备注说明</param>
        /// <param name="formData">表单数据</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult Create(bool isNew,string processId,string schemeCode,string processName,int processLevel,string description,string auditers, string formData) {
            UserInfo userInfo = LoginUserInfo.Get();
            string url = Config.GetValue("workflowapi");
            var data = new
            {
                isNew = isNew,
                processId = processId,
                schemeCode = schemeCode,
                processName = processName,
                processLevel = processLevel,
                description = description,
                formData = formData,
                auditers = auditers
            };
            var req = new {
                token = userInfo.token,
                loginMark = userInfo.loginMark,
                data = data.ToJson()
            };

            url = url + "/workflow/create";
            string res = HttpMethods.Post(url, req.ToJson());
            return Content(res);
        }

        /// <summary>
        /// 创建流程实例
        /// </summary>
        /// <param name="taskId">流程实例ID</param>
        /// <param name="verifyType">流程模板编码</param>
        /// <param name="description">流程实例名称</param>
        /// <param name="auditorId">加签人员Id</param>
        /// <param name="auditorName">备注说明</param>
        /// <param name="formData">表单数据</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult Audit(string taskId, string verifyType, string description, int auditorId, string auditorName,string auditers, string formData)
        {
            UserInfo userInfo = LoginUserInfo.Get();
            string url = Config.GetValue("workflowapi");
            var data = new
            {
                taskId = taskId,
                verifyType = verifyType,
                description = description,
                auditorId = auditorId,
                auditorName = auditorName,
                formData = formData,
                auditers = auditers
            };
            var req = new
            {
                token = userInfo.token,
                loginMark = userInfo.loginMark,
                data = data.ToJson()
            };

            url = url + "/workflow/audit";
            string res = HttpMethods.Post(url, req.ToJson());
            return Content(res);
        }
        #endregion

    }
}