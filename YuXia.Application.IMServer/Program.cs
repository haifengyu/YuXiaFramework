﻿using YuXia.Application.IM;

namespace YuXia.Application.IMServer
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创建人：雨夏-框架开发组
    /// 日 期：2017.04.01
    /// 描 述：程序开始入口
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Startup.Start();
        }
    }
}
