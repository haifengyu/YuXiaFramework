﻿using YuXia.Application.Form;
using Nancy;
using System.Collections.Generic;

namespace YuXia.Application.WebApi.Modules
{
    /// <summary>
    /// 版 本 YuXia-ADMS V6.1.6.0 雨夏敏捷开发框架
    /// Copyright (c) 2013-2017 上海雨夏软件科技有限公司
    /// 创建人：雨夏-框架开发组
    /// 日 期：2018.01.03
    /// 描 述：自定义表单处理接口
    /// </summary>
    public class FormApi: BaseApi
    {
        /// <summary>
        /// 注册接口
        /// </summary>
        public FormApi()
            : base("/YuXia/adms/form")
        {
            Get["/scheme"] = GetScheme;
            Get["/data"] = GetData;

            Post["/save"] = Save;
        }
        private FormSchemeIBLL formSchemeIBLL = new FormSchemeBLL();

        /// <summary>
        /// 获取表单模板数据
        /// </summary>
        /// <param name="_"></param>
        /// <returns></returns>
        private Response GetScheme(dynamic _)
        {
            string req = this.GetReqData();// 获取模板请求数据
            string[] formIds = req.Split(',');
            List<FormSchemeEntity> schemeList = new List<FormSchemeEntity>();
            foreach (var formId in formIds) {
                FormSchemeInfoEntity schemeInfoEntity = formSchemeIBLL.GetSchemeInfoEntity(formId);
                FormSchemeEntity schemeEntity = formSchemeIBLL.GetSchemeEntity(schemeInfoEntity.F_SchemeId);
                schemeList.Add(schemeEntity);
            }
            return Success(schemeList);
        }
        /// <summary>
        /// 获取自定义表单数据
        /// </summary>
        /// <param name="_"></param>
        /// <returns></returns>
        public Response GetData(dynamic _)
        {
            List<FormParam> req = this.GetReqData<List<FormParam>>();// 获取模板请求数据
            Dictionary<string, object> dic = new Dictionary<string, object>();
            foreach (var item in req)
            {
                if (string.IsNullOrEmpty(item.processIdName))
                {
                    var data = formSchemeIBLL.GetInstanceForm(item.schemeInfoId, item.keyValue);
                    dic.Add(item.schemeInfoId, data);
                }
                else
                {
                    var data = formSchemeIBLL.GetInstanceForm(item.schemeInfoId, item.processIdName, item.keyValue);//
                    dic.Add(item.schemeInfoId, data);
                }
            }
            return Success(dic);

        }


        /// <summary>
        ///  保存表单数据
        /// </summary>
        /// <param name="_"></param>
        /// <returns></returns>
        private Response Save(dynamic _)
        {
            List<FormParam> req = this.GetReqData<List<FormParam>>();// 获取模板请求数据
            foreach (var item in req)
            {
                formSchemeIBLL.SaveInstanceForm(item.schemeInfoId, item.processIdName, item.keyValue, item.formData);
            }
            return Success("保存成功");
        }
    }

    /// <summary>
    /// 自定义表单提交参数
    /// </summary>
    public class FormParam
    {
        /// <summary>
        /// 流程模板id
        /// </summary>
        public string schemeInfoId { get; set; }
        /// <summary>
        /// 关联字段名称
        /// </summary>
        public string processIdName { get; set; }
        /// <summary>
        /// 数据主键值
        /// </summary>
        public string keyValue { get; set; }
        /// <summary>
        /// 表单数据
        /// </summary>
        public string formData { get; set; }

    }
}